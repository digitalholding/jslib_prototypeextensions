if (!Array.prototype.max) {
    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function (fun /*, thisp*/) {
            var len = this.length;
            if (typeof fun != "function")
                throw new TypeError();

            var thisp = arguments[1];
            for (var i = 0; i < len; i++) {
                if (i in this)
                    fun.call(thisp, this[i], i, this);
            }
        };
    }

    Array.prototype.max = function (fun) {
        var max = null;
        for (var i = 0; i < this.length; i++) {
            var v = fun(this[i]);
            if (max == null || v > max) {
                max = v;
            }
        }
        return max;
    }
    Array.prototype.min = function (fun) {
        var min = null;
        for (var i = 0; i < this.length; i++) {
            var v = fun(this[i]);
            if (min == null || v > min) {
                min = v;
            }
        }
        return min;
    }
    Array.prototype.sum = function (fun) {
        var ret = 0;
        for (var i = 0; i < this.length; i++) {
            ret += fun(this[i]);
        }
        return ret;
    }
}